//
//  BNRChangeDateViewController.h
//  Homepwner
//
//  Created by qingpei on 1/7/15.
//  Copyright (c) 2015 qingpei. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BNRItem;
@interface BNRChangeDateViewController : UIViewController

@property(nonatomic, strong) BNRItem *item;

@end
