//
//  BNRDetailViewController.m
//  Homepwner
//
//  Created by qingpei on 1/6/15.
//  Copyright (c) 2015 qingpei. All rights reserved.
//

#import "BNRDetailViewController.h"
#import "BNRItem.h"
#import "BNRChangeDateViewController.h"

@interface BNRDetailViewController ()

@property(weak, nonatomic) IBOutlet UITextField *nameField;
@property(weak, nonatomic) IBOutlet UITextField *serialNumberField;
@property(weak, nonatomic) IBOutlet UITextField *valueField;
@property(weak, nonatomic) IBOutlet UILabel *dateLabel;
@property(weak, nonatomic) IBOutlet UIButton *changeDateButton;

@end

@implementation BNRDetailViewController

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];

  BNRItem *item = self.item;

  self.nameField.text = item.itemName;
  self.serialNumberField.text = item.serialNumber;
  self.valueField.text = [NSString stringWithFormat:@"%d", item.valueInDollars];

  // You need an NSDateFormatter that will turn a date into a simple date string
  static NSDateFormatter *dateFormatter = nil;
  if (!dateFormatter) {
    dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    dateFormatter.timeStyle = NSDateFormatterNoStyle;
  }

  // Use filtered NSDate object to set dateLabel contents
  self.dateLabel.text = [dateFormatter stringFromDate:item.dateCreated];
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];

  // Clear first responder
  [self.view endEditing:YES];

  // "Save" changes to item
  BNRItem *item = self.item;
  item.itemName = self.nameField.text;
  item.serialNumber = self.serialNumberField.text;
  item.valueInDollars = [self.valueField.text intValue];
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)setItem:(BNRItem *)item {
  _item = item;
  self.navigationItem.title = _item.itemName;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
  [self.nameField resignFirstResponder];
  [self.serialNumberField resignFirstResponder];
  [self.valueField resignFirstResponder];
}

- (IBAction)changeDate:(id)sender {
  BNRChangeDateViewController *cdvc =
      [[BNRChangeDateViewController alloc] init];
  cdvc.item = self.item;
  [self.navigationController pushViewController:cdvc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little
preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
