//
//  BNRChangeDateViewController.m
//  Homepwner
//
//  Created by qingpei on 1/7/15.
//  Copyright (c) 2015 qingpei. All rights reserved.
//

#import "BNRChangeDateViewController.h"
#import "BNRItem.h"

@interface BNRChangeDateViewController ()
@property(weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@end

@implementation BNRChangeDateViewController

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];

  self.datePicker.date = self.item.dateCreated;
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];

  self.item.dateCreated = self.datePicker.date;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little
preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
